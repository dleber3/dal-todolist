package listItemPanel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

/**
 * 
 */

/**
 * Implements a standard list-display on the left with Add/Modify/Remove on the right.
 * Should resize gracefully.
 * Note - ListItemPanel should only contain items of the same type.
 * @author David Leber
 * @version 1.0
 */
@SuppressWarnings("serial")
public class ListItemPanel extends JPanel {

	private DefaultListModel list;
	private JList jList;
	private JScrollPane scroll;
	private JButton buttonAdd, buttonModify, buttonRemove;
	
	private ListItemFactory factory;
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Test Frame");
		ArrayList<ListItem> list = new ArrayList<ListItem>();
		list.add(new TestListItem());
		list.add(new TestListItem());
		list.add(new TestListItem());
		frame.add(new ListItemPanel(list, new DefaultListItemFactory()));
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public ListItemPanel(ArrayList<ListItem> listToAdd, ListItemFactory factory) {
		assert factory != null;
		this.factory = factory;
		if (listToAdd == null) {
			listToAdd = new ArrayList<ListItem>();
		}
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints constraintsScroll = new GridBagConstraints();
		constraintsScroll.fill=GridBagConstraints.BOTH;
		constraintsScroll.weightx=1;
		constraintsScroll.weighty=1;
		constraintsScroll.insets = new Insets(20, 20, 20, 20);
		
		
		list = new DefaultListModel();
		for (ListItem cur : listToAdd) {
			list.addElement(cur);
		}
		
		jList = new JList(list);
		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		
		scroll = new JScrollPane(jList);
		scroll.setPreferredSize(new Dimension(200,200));
		this.add(scroll, constraintsScroll);
		
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		buttonAdd = new JButton("Add");
		buttonModify = new JButton("Modify");
		buttonRemove = new JButton("Remove");
		buttonAdd.addActionListener(new ButtonListener());
		buttonModify.addActionListener(new ButtonListener());
		buttonRemove.addActionListener(new ButtonListener());
		
		right.add(buttonAdd);
		right.add(buttonModify);
		right.add(buttonRemove);
		
		GridBagConstraints constraintsRight = new GridBagConstraints();
		constraintsRight.gridx=1;
		constraintsRight.gridy=0;
		constraintsRight.insets = new Insets(20, 0, 20, 20);
		constraintsRight.anchor = GridBagConstraints.NORTH;
		this.add(right, constraintsRight);
	}
	
	public void setEditVisible(boolean show) {
		buttonModify.setVisible(show);
	}

	public ArrayList<ListItem> getList() {
		ArrayList<ListItem> out = new ArrayList<ListItem>();
		for (int i=0; i<list.size(); i++) {
			out.add((ListItem)list.get(i));
		}
		return out;
	}
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			ListItem item;
			
			if (e.getSource()==buttonAdd) {
				item = factory.addDialog();
				if (item != null)
				{
					list.addElement(item);
				}
				return;
			} else {
				item = (ListItem)jList.getSelectedValue();
				if (item==null) {
					item = (ListItem)list.get(0);
				}
				
				if (e.getSource()==buttonModify) {
					item.modifyDialog();
				} else if (e.getSource()==buttonRemove) {
					if (item.removeDialog()) {
						list.removeElement(item);
					}
				} else {
					throw new RuntimeException("You need to an a listener to that new button");
				}
			}
		}

	}

}
