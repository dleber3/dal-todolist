package listItemPanel;

public class TestListItem implements ListItem {
	
	@Override
	public void modifyDialog() {
		System.out.println("Modify Dialog");
	}

	@Override
	public boolean removeDialog() {
		System.out.println("Remove Dialog");
		return true;
	}

}
