package listItemPanel;

/**
 * 
 */

/**
 * Interface used for items that can be displayed in a ListItemPanel with Add/Modify/Remove buttons.
 * @author dleber
 *
 */
public interface ListItem {

	
	/**
	 * Creates a dialog that prompts the user to edit the item.
	 */
	void modifyDialog();
	
	/**
	 * Creates a dialog that prompts the user to confirm deletion of the item.
	 * @return True to remove from list.
	 */
	boolean removeDialog();
	
}
