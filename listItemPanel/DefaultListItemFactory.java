/**
 * 
 */
package listItemPanel;

/**
 * @author David
 *
 */
public class DefaultListItemFactory implements ListItemFactory {

	/* (non-Javadoc)
	 * @see listItemPanel.ListItemFactory#addDialog()
	 */
	@Override
	public ListItem addDialog() {
		System.out.println("Add dialog.");
		return new TestListItem();
	}

}
