/**
 * 
 */
package listItemPanel;

/**
 * @author David
 *
 */
public interface ListItemFactory {

	/**
	 * Creates a dialog that prompts the user to create a new item of the same time as was clicked.
	 * Note - the panel should only contain items of the same type.
	 * @return The new item to be added to the list.
	 */
	ListItem addDialog();
	
}
