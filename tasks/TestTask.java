/**
 * 
 */
package tasks;

import java.io.*;

import static org.junit.Assert.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

/**
 * @author David
 *
 */
public class TestTask {

	Task test;
	
	@Before
	public void startUp() {
		test = new Task("Test description", 1,1,2010);
	}
	
	@Test
	public void testInstantiate() {
		assertNotNull(test);
		assertEquals("Description not set.", "Test description", test.description);
	}
	
	@Test
	public void testStartDate() {
		Calendar c1 = new GregorianCalendar();
		int day = c1.get(Calendar.DATE);
		int month = c1.get(Calendar.MONTH);
		int year = c1.get(Calendar.YEAR);
		assertEquals(new TaskDate(month, day, year).getDate(), test.start.getDate());
	}
	
	@Test 
	public void testDueDate() {
		assertEquals(new TaskDate(1,1,2010).getDate(), test.due.getDate());
	}
	
	@Test
	public void testFinish() {
		assertFalse("Task shouldn't be finished yet.", test.isFinished);
		test.finish();
		assertTrue("Task should be finished.", test.isFinished);
	}
	
	@Test
	public void testFinishDate() {
		assertNull("Shouldn't have a finished date yet.", test.finished);
		test.finish();
		assertEquals("Incorrect finished date.", new TaskDate().getDate(), test.finished.getDate());
	}
	
	@Test
	public void testCompare() {
		Task first = new Task("", 1, 1, 2009);
		Task second = new Task("", 1, 2, 2010);
		Task third = new Task("", 1, 3, 2011);
		
		assertTrue(first.compareTo(second)<0);
		assertTrue(third.compareTo(second)>0);
		assertTrue(second.compareTo(second)==0);
	}
	
	@Test
	public void testReadAndWrite() {
		File file = new File("debug.txt");
		try {
			Task test2 = new Task("Description 2", 1, 1, 2010);
			BufferedWriter out = new BufferedWriter(new PrintWriter(file));
			test.finish();
			test.print(out);
			test2.print(out);
			out.flush();
			out.close();
			
			Scanner in = new Scanner(file);
			
			Task testRead = Task.readIn(in);
			Task testRead2 = Task.readIn(in);
			
			assertEquals("Description doesn't match", test.description, testRead.description);
			assertEquals("Started doesn't match", test.start.toString(), testRead.start.toString());
			assertEquals("Due doesn't match", test.due.toString(), testRead.due.toString());
			assertEquals("Finished doesn't match", test.finished.toString(), testRead.finished.toString());
			assertTrue("isFinished is wrong", testRead.isFinished);
			
			assertEquals("Description 2 doesn't match", test2.description, testRead2.description);
			assertEquals("Started 2 doesn't match", test2.start.toString(), testRead2.start.toString());
			assertEquals("Due 2 doesn't match", test2.due.toString(), testRead2.due.toString());
			assertEquals("Finished 2 doesn't match", test2.finished, testRead2.finished);
			assertFalse("isFinished is wrong", testRead2.isFinished);
			
			
			
		} catch (FileNotFoundException e) {
			fail("Couldn't make the file - learn to IO right!");
		} catch (IOException e) {
			fail("Task.print() threw an IO exception");
			e.printStackTrace();
		}
		file.delete();
	}

}
