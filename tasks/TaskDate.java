/**
 * 
 */
package tasks;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * @author David
 *
 */
@SuppressWarnings("rawtypes")
public class TaskDate implements Comparable {

	int month;
	int day;
	int year;
	
	public TaskDate(int month, int day, int year) {
		//TODO: Check for invalid dates.
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	public TaskDate(String date) {
		String[] contents = new String[3];
		
		for (int i = 0; i<2; i++) {
			contents[i] = date.substring(0, date.indexOf('/'));
			date = date.substring(date.indexOf('/')+1);
		}
		contents[2] = date;
		
		this.month = Integer.parseInt(contents[0]);
		this.day = Integer.parseInt(contents[1]);
		this.year = Integer.parseInt(contents[2]);
		
		if (year<100) {
			year = year+2000;
		}
		
	}
	
	public TaskDate() {
		this(1,1,2010);
		Calendar c1 = new GregorianCalendar();
		this.day = c1.get(Calendar.DATE);
		this.month = c1.get(Calendar.MONTH);
		this.year = c1.get(Calendar.YEAR);
	}
	
	public int getDate() {
		return day+100*(month+100*year);
	}

	@Override
	public int compareTo(Object other) {
		if (other instanceof TaskDate) {
			TaskDate o = (TaskDate) other;
			return this.getDate()-o.getDate();
		}
		return 0;
	}
	
	@Override
	public String toString() {
		return this.month+"/"+this.day+"/"+this.year;
	}
	
}
