/**
 * 
 */
package tasks;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import listItemPanel.ListItem;

/**
 * @author David
 *
 */
@SuppressWarnings("rawtypes")
public class Task implements Comparable, ListItem {

	//TODO: Add overdue check.
	
	String description;
	TaskDate due, start, finished;
	boolean isFinished;
	
	public Task(String description, int month, int day, int year) {
		this.description = description;
		start = new TaskDate();
		due = new TaskDate(month, day, year);
		isFinished = false;
	}
	
	public Task(String description, TaskDate start, TaskDate due,
			TaskDate finished) {
		this.description = description;
		this.due = due;
		this.start = start;
		this.finished = finished;
		if (this.finished!=null) {
			this.isFinished = true;
		} else {
			this.isFinished = false;
		}
	}

	public Task(String description, String date) {
		this.description = description;
		this.start = new TaskDate();
		this.due = new TaskDate(date);
		isFinished = false;
	}

	public void finish() {
		isFinished = true;
		finished = new TaskDate();
	}

	@Override
	public int compareTo(Object arg0) {
		return this.due.compareTo(((Task)arg0).due);
	}

	@Override
	public void modifyDialog() {
		// TODO Actually allow someone to modify  tasks.
		JOptionPane.showMessageDialog(null, "Modify NYI", "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public boolean removeDialog() {
		int choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove "+this.toString());
		if (choice==JOptionPane.YES_OPTION) {
			return true;
		} else {
			return false;
		}
	}
	
	public void print(BufferedWriter out) throws IOException {
		out.write(description+"|"+start+"|"+due+"|"+finished+"\n");
	}
	
	public static Task readIn(Scanner in) {
		String line = in.nextLine();
		String[] contents = new String[4];
		
		for (int i = 0; i<3; i++) {
			int loc = line.indexOf('|');
			contents[i] = line.substring(0, loc);
			line = line.substring(line.indexOf('|')+1);
		}
		contents[3] = line;
		
		TaskDate start = new TaskDate(contents[1]);
		TaskDate due = new TaskDate(contents[2]);
		TaskDate finished;
		if (contents[3].equals("null")) {
			finished = null;
		} else {
			finished = new TaskDate(contents[3]);
		}
		
		return new Task(contents[0], start, due, finished);
	}
	
	public String toString() {
		int numToShow = 20;
		
		if (numToShow>description.length()) {
			return due.toString()+" "+description;
		} else {
			return due.toString()+" "+description.substring(0, numToShow)+"...";
		}
	}
	
}
