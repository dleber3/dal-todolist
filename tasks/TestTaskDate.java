/**
 * 
 */
package tasks;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

/**
 * @author David
 *
 */
public class TestTaskDate {
	
	@Test
	public void testInstantiate() {
		TaskDate date = new TaskDate(1,1,2010);
		assertNotNull("Couldn't instantiate task with dates.", date);
		assertEquals("Month assigned incorrectly. ", date.month, 1);
		assertEquals("Day assigned incorrectly. ", date.day, 1);
		assertEquals("Year assigned incorrectly. ", date.year, 2010);
	}
	
	@Test
	public void testToday() {

		Calendar c1 = new GregorianCalendar();
		int day = c1.get(Calendar.DATE);
		int month = c1.get(Calendar.MONTH);
		int year = c1.get(Calendar.YEAR);
		TaskDate test = new TaskDate();
		TaskDate test2 = new TaskDate(month, day, year);
		assertEquals("Today's date is calculated incorrectly.", test2.getDate(), test.getDate());
	}
	
	@Test
	public void correctDueDate() {
		TaskDate task = new TaskDate(1,1,2010);
		assertEquals("Task doesn't output the right date.", 20100101, task.getDate());
	}
	
	@Test
	public void testCompare() {
		TaskDate d1 = new TaskDate(1,1,2010);
		TaskDate d2 = new TaskDate(1,2,2010);
		TaskDate d3 = new TaskDate(1,3,2010);
		assertTrue("Less than test failed.", d1.compareTo(d2)<0);
		assertTrue("Greater than test failed.", d3.compareTo(d2)>0);
		assertTrue("Equal to test failed.", d2.compareTo(d2)==0);
	}
	
	@Test
	public void testAltConstructor() {
		TaskDate d1 = new TaskDate("1/1/10");
		TaskDate d2 = new TaskDate("2/2/2010");
		
		assertEquals("Day not assigned right.", 1, d1.day);
		assertEquals("Month not assigned right.", 2, d2.month);
		assertEquals("Year not assigned right for abbrev.", 2010, d1.year);
		assertEquals("Year not assigned right for not abbrev.", 2010, d2.year);
	}
	
	@Test
	public void testToString() {
		TaskDate d1 = new TaskDate(1,1,2010);
		assertEquals("toString() didn't work right.", "1/1/2010", d1.toString());
	}
	
}
