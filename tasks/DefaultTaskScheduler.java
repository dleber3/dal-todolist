/**
 * 
 */
package tasks;

//TODO: Add support to track completed tasks.

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.io.*;

import listItemPanel.ListItem;

/**
 * @author David
 *
 */
public class DefaultTaskScheduler implements TaskScheduler {
	
	PriorityQueue<Task> taskList;
	Task currentTask;

	public DefaultTaskScheduler() {
		taskList = new PriorityQueue<Task>();
	}
	
	/* (non-Javadoc)
	 * @see tasks.TaskScheduler#addTask(tasks.Task)
	 */
	@Override
	public void addTask(Task task) {
		taskList.add(task);
	}

	/* (non-Javadoc)
	 * @see tasks.TaskScheduler#finishTask(tasks.Task)
	 */
	@Override
	public void finishTask(Task task) {
		if (task!=currentTask) {
			throw new RuntimeException("Passed task is not currentTask.");
		}
		
		task.finish();
		currentTask = null;
	}

	/* (non-Javadoc)
	 * @see tasks.TaskScheduler#getNewTask()
	 */
	@Override
	public Task getNewTask() throws TaskSchedulerException {
		if (currentTask!=null) {
			throw new TaskAlreadyOpenException();
		}
		
		if (taskList.size()==0) {
			throw new NoTasksAvailableException();
		}
		
		this.currentTask = taskList.poll();
		return this.currentTask;
	}

	/* (non-Javadoc)
	 * @see tasks.TaskScheduler#getCurrentTask()
	 */
	@Override
	public Task getCurrentTask() {
		return currentTask;
	}

	@Override
	public ArrayList<ListItem> getTaskList() {
		Iterator<Task> it = taskList.iterator();
		ArrayList<ListItem> list = new ArrayList<ListItem>();
		
		while (it.hasNext()) {
			list.add(it.next());
		}
		
		if (currentTask!=null) {
			System.out.println(list.contains(currentTask));
			list.add(currentTask);
		}
		
		return list;
	}

	@Override
	public void setTaskList(ArrayList<ListItem> newTaskList) {
		taskList = new PriorityQueue<Task>();
		
		for (ListItem cur : newTaskList) {
			Task curTask = (Task)cur;
			if (curTask != currentTask) {
				taskList.add(curTask);
			}
		}
	}
	
	//Idle means there was no active task when the system was closed. Active means there was.
	static final String IDLE = "idle", ACTIVE = "active", FILENAME="taskList.txt";

	@Override
	public void saveTasks() throws IOException {
		BufferedWriter out = new BufferedWriter(new PrintWriter(new File(FILENAME)));
		
		if (currentTask==null) {
			out.write(IDLE);
			out.newLine();
		} else {
			out.write(ACTIVE);
			out.newLine();
			currentTask.print(out);
		}
		
		for (Task cur : taskList) {
			cur.print(out);
		}
		out.close();
	}

	@Override
	public void loadTasks() throws FileNotFoundException {
		Scanner file = new Scanner(new File(FILENAME));
		
		if (!file.hasNext()) {	//if the file is empty, don't load any tasks.
			return;
		}
		
		String line = file.nextLine();
		if (line.equals(ACTIVE)) {
			currentTask = Task.readIn(file);
		}
		
		while (file.hasNext()) {
			taskList.add(Task.readIn(file));
		}
		file.close();
	}

}
