/**
 * 
 */
package tasks;

import static org.junit.Assert.*;

import java.util.ArrayList;

import listItemPanel.ListItem;

import org.junit.Before;
import org.junit.Test;
import tasks.TaskScheduler.TaskSchedulerException;

/**
 * @author David
 *
 */
public class TestDefaultTaskScheduler {

	DefaultTaskScheduler test;
	Task first, second, third;
	
	@Before
	public void setUp() {
		test = new DefaultTaskScheduler();
		first = new Task("", 1, 1, 2010);
		second = new Task("", 1, 2, 2010);
		third = new Task("", 1, 3, 2011);
	}
	
	@Test
	public void initialize() {
		assertNotNull("Didn't instantiate.", test);
	}

	@Test
	public void testAdd() {
		assertEquals("Task list doesn't start empty.", null, test.taskList.poll());
		test.addTask(first);
		assertEquals("Task wasn't added to list.", first, test.taskList.poll());
	}
	
	@Test
	public void testList() {
		test.addTask(first);
		assertEquals("Task didn't add right.", first, test.taskList.poll());
		test.addTask(third);
		test.addTask(second);
		assertEquals("Tasks don't sort by priority.", second, test.taskList.poll());
		assertEquals("Tasks are getting lost.", third, test.taskList.poll());
		assertEquals("Tasks are getting created.", null, test.taskList.poll());
	}
	
	@Test (expected=DefaultTaskScheduler.TaskAlreadyOpenException.class)	//Fails if you try to get a new task but one is already open.
	public void testGetNewTask() throws TaskSchedulerException{
		assertNull("currentTask isn't null to start.", test.currentTask);
		first = new Task("", 1, 1, 2010);
		second = new Task("", 1, 2, 2010);
		test.addTask(first);
		test.addTask(second);
		Task current = test.getNewTask();
		assertEquals("Scheduler didn't return the right task.", first, current);
		assertEquals("Task wasn't set to current task.", first, test.currentTask);
		test.getNewTask();
	}
	
	@Test (expected = TaskScheduler.NoTasksAvailableException.class)
	public void testGetTaskWhenNone() throws TaskSchedulerException{
		test.getNewTask();
	}
	
	@Test
	public void testGetCurrentTask() throws TaskSchedulerException {
		assertNull("currentTask isn't null to start.", test.getCurrentTask());
		first = new Task("", 1, 1, 2010);
		test.addTask(first);
		test.getNewTask();
		Task current = test.getCurrentTask();
		assertEquals("Current task isn't right.", first, current);
	}
	
	@Test
	public void finishTask() throws TaskSchedulerException {
		first = new Task("", 1, 1, 2010);
		second = new Task("", 1, 2, 2010);
		test.addTask(first);
		test.addTask(second);
		test.getNewTask();
		assertEquals("DefaultTaskScheduler.currentTask isn't working.", first, test.currentTask);
		assertFalse("Task is already finished?", first.isFinished);
		test.finishTask(first);
		assertTrue("Task didn't get finished.", first.isFinished);
		assertNull("TaskScheduler didn't clear current task.", test.currentTask);
	}
	
	@Test
	public void testGetTaskList() {
		test.addTask(first);
		test.addTask(second);
		test.addTask(third);
		ArrayList<ListItem> list = test.getTaskList();
		
		assertEquals("List isn't the right length", 3, list.size());
		assertTrue("List doesn't have all elements", list.contains(first));
		assertTrue("List doesn't have all elements", list.contains(second));
		assertTrue("List doesn't have all elements", list.contains(third));
		
	}
}
