/**
 * 
 */
package tasks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import listItemPanel.ListItem;


/**
 * @author David
 *
 */
@SuppressWarnings("serial")
public interface TaskScheduler {

	public void addTask(Task task);
	
	public void finishTask(Task task);
	
	public Task getNewTask() throws TaskSchedulerException;
	
	public Task getCurrentTask();
	
	public ArrayList<ListItem> getTaskList();
	
	public void setTaskList(ArrayList<ListItem> taskList);
	
	public void saveTasks() throws IOException;
	
	public void loadTasks() throws FileNotFoundException;
	
	public class TaskSchedulerException extends Exception {
		public TaskSchedulerException(String msg) {
			super(msg);
		}
	}
	
	public class TaskAlreadyOpenException extends TaskSchedulerException {
		public TaskAlreadyOpenException() {
			super("Tried to open a task, but one was already open. Finish it first.");
		}
	}
	public class NoTasksAvailableException extends TaskSchedulerException {
		public NoTasksAvailableException() {
			super("Tried to open a task, but no tasks available.");
		}
	}
	
}
