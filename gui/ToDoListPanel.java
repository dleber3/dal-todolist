/**
 * 
 */
package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.*;
import tasks.*;
import tasks.TaskScheduler.TaskSchedulerException;

/**
 * @author David
 *
 */
@SuppressWarnings("serial")
public class ToDoListPanel extends JPanel {
	
	TaskScheduler scheduler;
	Task currentTask;
	
	JPanel buttonsPanel;
	JLabel currentTaskLabel;
	JButton addButton, getButton, finishButton;
	
	static final String ADD = "add";
	static final String GET = "get";
	static final String FINISH = "finish";
	
	public static void main(String[] args) {
		new ToDoListPanel();
	}
	
	public ToDoListPanel() {
		JFrame frame = new JFrame("ToDoListPanel");
		
		scheduler = new DefaultTaskScheduler();
		try {
			scheduler.loadTasks();
		} catch (FileNotFoundException e) {
			// If the file isn't found, don't do anything.
		}
		
		instantiateComponents();
		addComponents();
		
		frame.add(this);
		frame.addWindowListener(new ExitListener());
		frame.setVisible(true);
		frame.pack();
		
	}

	private void instantiateComponents() {
		currentTaskLabel = new JLabel();
		resetCurrentTask();
		buttonsPanel = new JPanel();
		
		addButton = new JButton("Add a Task");
		addButton.addActionListener(new ButtonListener(ADD));
		getButton = new JButton("Get a Task");
		getButton.addActionListener(new ButtonListener(GET));
		finishButton = new JButton("Finish a Task");
		finishButton.addActionListener(new ButtonListener(FINISH));
	}

	private void addComponents() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		this.add(currentTaskLabel);
		currentTaskLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(buttonsPanel);
		
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		buttonsPanel.add(addButton);
		buttonsPanel.add(getButton);
		buttonsPanel.add(finishButton);
	}
	
	private void resetCurrentTask() {
		currentTask = scheduler.getCurrentTask();
		if (currentTask==null) {
			currentTaskLabel.setText("Current Task: None");
		} else {
			currentTaskLabel.setText("Current Task: "+currentTask.toString());
		}
	}

	private class ButtonListener implements ActionListener {

		String message;
		
		public ButtonListener(String message) {
			this.message = message;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (message == ADD) {
				AddPanel addPanel = new AddPanel(scheduler);
				JOptionPane.showMessageDialog(null, addPanel);
				scheduler.setTaskList(addPanel.getTaskList());
			} else if (message == GET) {
				try {
					scheduler.getNewTask();
					resetCurrentTask();
				} catch (TaskSchedulerException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error occurred", JOptionPane.ERROR_MESSAGE);
				}
			} else if (message == FINISH) {
				if (currentTask == null) {
					JOptionPane.showMessageDialog(null, "No current task, can't finish it", "Finish Error", JOptionPane.ERROR_MESSAGE);
				}
				int choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to finish "+scheduler.getCurrentTask().toString()+"?\nIt can't be restarted.");
				if (choice==JOptionPane.YES_OPTION) {
					scheduler.finishTask(currentTask);
					resetCurrentTask();
				}
			}
		}
		
	}
	
	private class ExitListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent arg0) {
			// Nothing here
		}

		@Override
		public void windowClosed(WindowEvent arg0) {
			// Nothing here
			
		}

		@Override
		public void windowClosing(WindowEvent arg0) {
			try {
				scheduler.saveTasks();
				System.exit(0);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, e.getStackTrace(), "Error Saving Tasks", JOptionPane.ERROR_MESSAGE);
			}
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// Nothing here
			
		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// Nothing here
			
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			// Nothing here
			
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			// Nothing here
			
		}
	}
}
