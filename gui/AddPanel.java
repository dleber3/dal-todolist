/**
 * 
 */
package gui;

import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import tasks.*;
import listItemPanel.ListItem;
import listItemPanel.ListItemFactory;
import listItemPanel.ListItemPanel;

/**
 * @author David
 *
 */
@SuppressWarnings("serial")
public class AddPanel extends JPanel {


	ListItemPanel panel;
	TaskScheduler scheduler;

	public AddPanel(TaskScheduler scheduler) {
		
		this.scheduler = scheduler;
		
		instantiateComponents();
		addComponents();
		
	}

	private void instantiateComponents() {
		
		ArrayList<ListItem> toAdd = scheduler.getTaskList();
		panel = new ListItemPanel(toAdd, new TaskFactory());
		
	}

	private void addComponents() {
		
		this.add(panel);
	}	
	
	
	private class TaskFactory implements ListItemFactory {

		JPanel panel, top, bottom;
		JComboBox day, month, year;
		JTextField description;
		
		JLabel dateLabel, descriptionLabel, slashLabel, slashLabel2;
		
		@Override
		public ListItem addDialog() {
			
			panel = new JPanel();
			top = new JPanel();
			bottom = new JPanel();
			String[] dayOptions, monthOptions, yearOptions;
			
			dayOptions = getDayOptions();
			monthOptions = getMonthOptions();
			yearOptions = getYearOptions();
			
			day = new JComboBox(dayOptions);
			month = new JComboBox(monthOptions);
			year = new JComboBox(yearOptions);
			
			description = new JTextField();
			
			dateLabel = new JLabel("Due date: ");
			slashLabel = new JLabel("/");
			slashLabel2 = new JLabel("/");
			descriptionLabel = new JLabel("Description: ");
			
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			panel.add(top);
			panel.add(bottom);
			
			top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
			top.add(dateLabel);
			top.add(month);
			top.add(slashLabel);
			top.add(day);
			top.add(slashLabel2);
			top.add(year);
			
			bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
			bottom.add(descriptionLabel);
			bottom.add(description);
			
			int choice = JOptionPane.showConfirmDialog(null, panel, "Adding a Task", JOptionPane.OK_CANCEL_OPTION);
			if (choice==JOptionPane.OK_OPTION) {
				String date = month.getSelectedItem().toString()+"/"+day.getSelectedItem().toString()+"/"+year.getSelectedItem().toString();
				return new Task(description.getText(), date);
			}
			
			return null;
		}

		private String[] getYearOptions() {
			String[] out = new String[100];
			for (int i=0; i<100; i++) {
				out[i] = ""+(i+2011);
			}
			return out;
		}

		private String[] getMonthOptions() {
			String[] out = new String[12];
			for (int i=0; i<12; i++) {
				out[i] = ""+(i+1);
			}
			return out;
		}

		private String[] getDayOptions() {
			String[] out = new String[31];
			for (int i=0; i<31; i++) {
				out[i] = ""+(i+1);
			}
			return out;
		}

	}
	
	public ArrayList<ListItem> getTaskList() {
		return panel.getList();
	}
	
}
